// Create a database of hotel with a collection of rooms.
// Insert a single room (insertOne method) with the following details:


db.hotelRooms.insertOne({
    name: "Single",
    accomodates: 2,
    price: 1000,
    description:"A simple room with all the basic necessities",
    rooms_available:10,
    isAvailable: false
    
    });


// Insert multiple rooms (insertMany method) with the following details:

db.hotelRooms.insertMany([
    {
        name: "Double",
        accomodates: 3,
        price: 2000,
        description: "A room fit for a small family going on vacation",
        rooms_available: 5,
        isAvailable: false
        
        },
        
      {
        accomodates: 4,
        price: 4000,
        description: "A room with a queen sized bed perfect for a simple getaway",
        rooms_available: 15,
        isAvailable: false
        
        },
        
  ]);

   // Use the find method to search for a room with the name double.

   db.hotelRooms.find({name: "Double"});


   // Use the updateOne method to update the queen room and set the available rooms to 0.

   db.hotelRooms.updateOne(
    {accomodates: 4},
    {
        $set: {
            name: "Queen",
            accomodates: 4,
            price: 4000,
            description: "A room with a queen sized bed perfect for a simple getaway",
            rooms_available: 0,
            isAvailable: false
            }
        
        }
    );


   // Use the deleteMany method rooms to delete all rooms that have 0 availability.


   db.hotelRooms.deleteMany({
    rooms_available: 0
    });


